// Datatypes
// **********************1*********************
function backToFront(str, numberOfSymbols) {
    let strArr = str.split('');
    if(numberOfSymbols < strArr.length) {
        numberOfSymbols = strArr.slice(strArr.length - numberOfSymbols);
        let finalArr = numberOfSymbols.concat(strArr);
        let text = finalArr.join('');
        return text;
    } else return str;
}
console.log(backToFront('world', 4));

// **********************2*********************
function nearest(z, x, y) {
    if (x != y) {
        x1 = Math.abs(x - z);
        y1 = Math.abs(y - z);
        if (x1 < y1) {
        return x;
        }
        else if (y1 < x1) {
        return y;
        } else if (y1 == x1){
            return 0;
        } else return false;
    }
}
console.log(nearest(100, 22, 122));
console.log(nearest(50, 22, 122));


// **********************3*********************
function removeDuplicate(beginArr) {
    let finalArr = [];
    for(let i = 0; i < beginArr.length; i ++) {
        if (!finalArr.includes(beginArr[i])) {
            finalArr.push(beginArr[i]);
        }
    }
    return finalArr;
}
console.log(removeDuplicate([1,2,3,2,3,1,1]));
